#include <SDL.h>
#include <pango/pangocairo.h>

const char* text =
  "This is <b>some</b> <i>English</i> text\n"
  "هذه بعض النصوص العربية\n"
  "這是一些中文";

const char* font = "DejaVu Sans 24";

static void unpremultiply(guint8 *pixels, gint height, gint pitch)
{
    guint8 *end = pixels + height * pitch;

    for (guint32 *c = (guint32 *)pixels; c < (guint32 *)end; c++) {
        guint32
            a = (*c >> 24) & 0xff,
            r = (*c >> 16) & 0xff,
            g = (*c >>  8) & 0xff,
            b = (*c >>  0) & 0xff;

        if (a == 0) continue;

        *c = a << 24            |
            (r * 255 / a) << 16 |
            (g * 255 / a) << 8  |
            (b * 255 / a);
    }
}

typedef struct _TextField
{
    SDL_Texture     *texture;
    PangoLayout     *layout;
    int              n_revealed;

    double           fill_rgba[4];
    double           stroke_rgba[4];
    double           stroke_width;
} TextField;

TextField *text_field_new(SDL_Renderer *renderer, int w, int h)
{
    TextField *text_field = g_new(TextField, 1);
    SDL_Rect rect = {0, 0, w, h};

    text_field->texture = SDL_CreateTexture(renderer,
                                           SDL_PIXELFORMAT_BGRA32,
                                           SDL_TEXTUREACCESS_STREAMING,
                                           w, h);
    SDL_SetTextureBlendMode(text_field->texture, SDL_BLENDMODE_BLEND);

    void *pixels;
    int pitch;
    SDL_LockTexture(text_field->texture, NULL, &pixels, &pitch);
    cairo_surface_t *surface = cairo_image_surface_create_for_data(pixels,
                                                                   CAIRO_FORMAT_ARGB32,
                                                                   w, h, pitch);

    cairo_t *cr = cairo_create(surface);
    text_field->layout = pango_cairo_create_layout(cr);

    cairo_set_source_rgba(cr, 0.0, 0.0, 0.0, 0.0);
    cairo_rectangle(cr, 0, 0, w, h);
    cairo_fill(cr);

    text_field->fill_rgba[0] = 1.0;
    text_field->fill_rgba[1] = 1.0;
    text_field->fill_rgba[2] = 1.0;
    text_field->fill_rgba[3] = 1.0;
    text_field->stroke_rgba[0] = 0.0;
    text_field->stroke_rgba[1] = 0.0;
    text_field->stroke_rgba[2] = 0.0;
    text_field->stroke_rgba[3] = 1.0;
    text_field->stroke_width = 1.0;

    SDL_UnlockTexture(text_field->texture);
    cairo_surface_destroy(surface);
    cairo_destroy(cr);
    return text_field;
}

void text_field_destroy(TextField *text_field) {
    g_object_unref(text_field->layout);
    SDL_DestroyTexture(text_field->texture);
    g_free(text_field);
}

void text_field_refresh(TextField *text_field)
{
    SDL_Rect rect = {0, 0, 0, 0};
    void *pixels;
    int pitch;

    SDL_QueryTexture(text_field->texture, NULL, NULL, &rect.w, &rect.h);

    SDL_LockTexture(text_field->texture, &rect, &pixels, &pitch);
    cairo_surface_t *surface = cairo_image_surface_create_for_data(pixels,
                                                                   CAIRO_FORMAT_ARGB32,
                                                                   rect.w, rect.h, pitch);

    cairo_t *cr = cairo_create(surface);
    pango_cairo_update_context(cr, pango_layout_get_context(text_field->layout));
    pango_layout_context_changed(text_field->layout);
    pango_layout_set_width(text_field->layout, pango_units_from_double(rect.w));

    memset(pixels, 0, rect.h * pitch);

    PangoLayoutIter *iter = pango_layout_get_iter(text_field->layout);
    const char *text = pango_layout_get_text(text_field->layout);
    int n_shown = 0;
    do {
        int baseline = pango_layout_iter_get_baseline(iter);
        PangoLayoutLine *line = pango_layout_iter_get_line_readonly(iter);
        PangoRectangle logical_rect;
        pango_layout_iter_get_line_extents (iter, NULL, &logical_rect);
        int x_off = 0;

        switch (line->resolved_dir) {
        case PANGO_DIRECTION_RTL:
            line->runs = g_slist_reverse(line->runs);
        case PANGO_DIRECTION_LTR:
        {
            GSList *runs = line->runs;
            while (runs != NULL && n_shown < text_field->n_revealed) {
                PangoLayoutRun *run = (PangoLayoutRun *)runs->data;
                if (run == NULL)
                    continue;
                if (n_shown + run->item->num_chars > text_field->n_revealed) {
                    PangoGlyphItem *before, *after = pango_glyph_item_copy(run);
                    gchar *looking_at = g_utf8_substring(text + run->item->offset, 0, text_field->n_revealed - n_shown);
                    int split_index = strlen(looking_at);
                    g_free(looking_at);
                    before = pango_glyph_item_split(after, text, split_index);
                    cairo_move_to(cr,
                                  pango_units_to_double(x_off + logical_rect.x),
                                  pango_units_to_double(baseline + logical_rect.y));
                    pango_cairo_glyph_string_path(cr, run->item->analysis.font, before->glyphs);
                    pango_glyph_item_free(before);
                    pango_glyph_item_free(after);
                    n_shown = text_field->n_revealed;
                } else {
                    cairo_move_to(cr,
                                  pango_units_to_double(x_off + logical_rect.x),
                                  pango_units_to_double(baseline + logical_rect.y));
                    pango_cairo_glyph_string_path(cr, run->item->analysis.font, run->glyphs);
                    x_off += pango_glyph_string_get_width(run->glyphs);
                    n_shown += run->item->num_chars;
                }
                runs = runs->next;
            }
        }
        }
    } while (pango_layout_iter_next_line(iter) && n_shown < text_field->n_revealed);

    cairo_set_source_rgba(cr,
                          text_field->fill_rgba[0],
                          text_field->fill_rgba[1],
                          text_field->fill_rgba[2],
                          text_field->fill_rgba[3]);

    cairo_fill_preserve(cr);

    cairo_set_source_rgba(cr,
                          text_field->stroke_rgba[0],
                          text_field->stroke_rgba[1],
                          text_field->stroke_rgba[2],
                          text_field->stroke_rgba[3]);

    cairo_set_line_width (cr, text_field->stroke_width);
    cairo_stroke(cr);

    unpremultiply(pixels, rect.h, pitch);

    SDL_UnlockTexture(text_field->texture);
    cairo_surface_destroy(surface);
    cairo_destroy(cr);
}

int main () {
    /* Setup our SDL window */
    int width      = 800;
    int height     = 600;
    int videoFlags =   SDL_WINDOW_RESIZABLE  ;

    /* Initialize our SDL window */
    if(SDL_Init(SDL_INIT_VIDEO) < 0)   {
        fprintf(stderr, "Failed to initialize SDL");
        return -1;
    }

    SDL_Window *screen = SDL_CreateWindow(
        "SDL+Pango Example",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        width,
        height,
        videoFlags
    );

    SDL_Renderer *renderer = SDL_CreateRenderer(
        screen,
        -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
    );

    TextField *text_field = text_field_new(renderer, 800, 600);
    pango_layout_set_markup(text_field->layout, text, -1);
    PangoFontDescription *desc = pango_font_description_from_string(font);
    pango_layout_set_font_description(text_field->layout, desc);
    pango_font_description_free(desc);
    pango_layout_set_line_spacing(text_field->layout, 0.5);
    int n_chars_total = pango_layout_get_character_count(text_field->layout);
    text_field->n_revealed = n_chars_total;
    text_field->stroke_width = 0.25;
    text_field_refresh(text_field);

    /* Our main event/draw loop */
    int done = 0;

    while (!done) {
        /* Blit our new image to our visible screen */
        SDL_SetRenderDrawColor(renderer,235, 52, 146,0);
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer,text_field->texture,NULL,NULL);
        SDL_RenderPresent(renderer);

        /* Handle SDL events. Wait for the first event, process any others that
         * may also be pending. If you don't want to pause your program waiting
         * for events, use SDL_PollEvent instead. */
        SDL_Event event;
        if (SDL_WaitEvent(&event)) {
            do {
                switch (event.type) {
                    case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:
                            done = 1;
                            break;
                        case SDLK_RIGHT:
                            if (++text_field->n_revealed > n_chars_total)
                                text_field->n_revealed = n_chars_total;
                            text_field_refresh(text_field);
                            break;
                        case SDLK_LEFT:
                            if (--text_field->n_revealed < 0)
                                text_field->n_revealed = 0;
                            text_field_refresh(text_field);
                            break;
                        }
                        break;

                    case SDL_QUIT:
                        done = 1;
                        break;

                    case SDL_WINDOWEVENT:
                        switch (event.window.event) {
                            case SDL_WINDOWEVENT_SIZE_CHANGED:
                                width = event.window.data1;
                                height = event.window.data2;
                                SDL_DestroyTexture(text_field->texture);
                                text_field->texture = SDL_CreateTexture(renderer,
                                                                       SDL_PIXELFORMAT_ARGB8888,
                                                                       SDL_TEXTUREACCESS_STREAMING,
                                                                       width, height);
                                SDL_SetTextureBlendMode(text_field->texture, SDL_BLENDMODE_BLEND);
                                text_field_refresh(text_field);
                                break;
                            case SDL_WINDOWEVENT_CLOSE:
                                event.type = SDL_QUIT;
                                SDL_PushEvent(&event);
                                break;
                        }
                        break;
                }
            } while(SDL_PollEvent(&event));
        }
    }

    /* Cleanup */
    text_field_destroy(text_field);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(screen);
    SDL_Quit();

    return 0;
}
